local filesystem = require('gears.filesystem')

-- Thanks to jo148 on github for making rofi dpi aware!
local with_dpi = require('beautiful').xresources.apply_dpi
local get_dpi = require('beautiful').xresources.get_dpi
local rofi_command = 'env /usr/bin/rofi -dpi ' .. get_dpi() .. ' -width ' .. with_dpi(400) .. ' -show drun -theme ' .. filesystem.get_configuration_dir() .. '/configuration/rofi.rasi -run-command "/bin/bash -c -i \'shopt -s expand_aliases; {cmd}\'"'

return {
  -- List of apps to start by default on some actions
  default = {
    -- terminal = 'alacritty',
    terminal = 'konsole',
    rofi = rofi_command,
    lock = 'i3lock-fancy -g -t "Session Locked"',
    screenshot = 'flameshot screen',
    region_screenshot = 'flameshot gui',
    delayed_screenshot = 'sleep 5 ; flameshot gui',

    -- Editing these also edits the default program
    -- associated with each tag/workspace
    browser = 'env brave',
    editor = 'emacs', -- gui text editor
    social = 'env slack',
    game = rofi_command,
    files = 'dolphin',
    music = rofi_command
  }
}
