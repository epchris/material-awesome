local wibox = require('wibox')
local mat_list_item = require('widget.material.list-item')
local mat_slider = require('widget.material.slider')
local mat_icon = require('widget.material.icon')
local icons = require('theme.icons')
local watch = require('awful.widget.watch')
local dpi = require('beautiful').xresources.apply_dpi

local slider =
  wibox.widget {
  read_only = true,
  widget = mat_slider
}

local max_temp = 70
local min_temp = 23
watch(
  'bash -c "cat /sys/class/hwmon/hwmon0/temp1_input"',
  1,
  function(_, stdout)
    local temp = stdout:match('(%d+)')
    slider:set_value(((temp / 1000) - min_temp) / (max_temp - min_temp) * 100)
    collectgarbage('collect')
  end
)

local temperature_meter =
  wibox.widget {
  wibox.widget {
    icon = icons.thermometer,
    size = dpi(24),
    widget = mat_icon
  },
  slider,
  widget = mat_list_item
}

return temperature_meter
